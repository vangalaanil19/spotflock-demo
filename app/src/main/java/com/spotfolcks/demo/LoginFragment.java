package com.spotfolcks.demo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.wifi.hotspot2.pps.HomeSp;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.spotfolcks.demo.databinding.FragmentLoginBinding;
import com.spotfolcks.demo.databinding.FragmentLogintwoBinding;
import com.spotfolcks.demo.helper.AppPref;
import com.webappclouds.utilslib.interfaces.ServiceResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {

    public static final String TAG = "LoginFragment";

    private LoginActivity parent;

    FragmentLogintwoBinding fragmentLogintwoBinding;

    ProgressDialog progressDoalog;

    ServiceRequestInterface serviceRequestInterface;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parent = (LoginActivity) getActivity();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLogintwoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_logintwo, container, false);

        setLoginRegisterToggle();

        serviceRequestInterface = ServerApi.getRetrofitInstance().create(ServiceRequestInterface.class);


        loginServiceRequest();

        fragmentLogintwoBinding.etGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGenderDialog();
            }
        });


                setSignUp();





        return fragmentLogintwoBinding.getRoot();
    }


    private void setLoginRegisterToggle() {
        fragmentLogintwoBinding.tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentLogintwoBinding.layoutLogin.setVisibility(View.VISIBLE);
                fragmentLogintwoBinding.layoutRegister.setVisibility(View.GONE);
            }
        });


        fragmentLogintwoBinding.tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentLogintwoBinding.layoutLogin.setVisibility(View.GONE);
                fragmentLogintwoBinding.layoutRegister.setVisibility(View.VISIBLE);
            }
        });
    }


    private boolean isLoginValidate() {
        boolean isValid = false;
        if (!Utils.emailValidate(fragmentLogintwoBinding.etUsername.getText().toString())) {
            Utils.setSnackBar(fragmentLogintwoBinding.etUsername, getString(R.string._txt_error_username));
        } else if (fragmentLogintwoBinding.etLoginpassword.getText().toString().isEmpty()) {
            Utils.setSnackBar(fragmentLogintwoBinding.etUsername, parent.getResources().getString(R.string._txt_password));
        } else {
            isValid = true;
        }

        return isValid;
    }


    private boolean isSignUpValidate() {
        boolean isValid = false;

        if (fragmentLogintwoBinding.etName.getText().toString().isEmpty()) {
            Utils.setSnackBar(fragmentLogintwoBinding.etName, "Please neter the name");
        } else if ( !Utils.emailValidate(fragmentLogintwoBinding.etEmail.getText().toString())) {
            Utils.setSnackBar(fragmentLogintwoBinding.etEmail, "Please enter the email");
        } else if (fragmentLogintwoBinding.etPassword.getText().toString().isEmpty()) {
            Utils.setSnackBar(fragmentLogintwoBinding.etPassword, "Please enter the password");
        } else if (!fragmentLogintwoBinding.etPassword.getText().toString().equalsIgnoreCase(fragmentLogintwoBinding.etConfirmPassword.getText().toString())) {
            Utils.setSnackBar(fragmentLogintwoBinding.etConfirmPassword, "Password and confirm password should be same");
        } else if (fragmentLogintwoBinding.etPhone.getText().toString().length() < 10) {
            Utils.setSnackBar(fragmentLogintwoBinding.etPhone, "please enter a valid phone number");
        } else if (fragmentLogintwoBinding.etGender.getText().toString().isEmpty()) {
            Utils.setSnackBar(fragmentLogintwoBinding.etGender, "please select gender");
        } else {
            isValid = true;
        }

        return isValid;
    }


    private void setSignUp() {
        fragmentLogintwoBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSignUpValidate()) {
                    String name = fragmentLogintwoBinding.etName.getText().toString();
                    String email = fragmentLogintwoBinding.etEmail.getText().toString();
                    String password = fragmentLogintwoBinding.etPassword.getText().toString();
                    String confirm_password = fragmentLogintwoBinding.etConfirmPassword.getText().toString();
                    String phone = fragmentLogintwoBinding.etPhone.getText().toString();
                    String gender = fragmentLogintwoBinding.etGender.getText().toString();

                    SignUpRequest signUpRequest = new SignUpRequest();
                    signUpRequest.name = name;
                    signUpRequest.email = email;
                    signUpRequest.password = password;
                    signUpRequest.password_confirmation = confirm_password;
                    signUpRequest.mobile = phone;
                    signUpRequest.gender = gender;

                    progressDoalog = new ProgressDialog(parent);
                    progressDoalog.setMessage("Loading....");
                    progressDoalog.show();


                    Call<JsonElement> call = serviceRequestInterface.signUpRequest(signUpRequest);

                    call.enqueue(new Callback<JsonElement>() {
                        @Override
                        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                            progressDoalog.dismiss();
                            try {

                                if(response.isSuccessful()){
                                Log.d("resultJson", response.body().toString());
                                JSONObject jsonObject = new JSONObject(response.body().toString());
                                if (jsonObject.has("success")) {
                                    SignUpSuccessVo signUpSuccessVo = com.webappclouds.utilslib.Utils.getSpecificVo(jsonObject.toString(), SignUpSuccessVo.class);
                                    if (signUpSuccessVo.getSuccess()) {
                                        Utils.setSnackBar(fragmentLogintwoBinding.btnRegister, signUpSuccessVo.getMessage());

                                        clearFields();

                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                fragmentLogintwoBinding.layoutLogin.setVisibility(View.VISIBLE);
                                                fragmentLogintwoBinding.layoutRegister.setVisibility(View.GONE);
                                            }
                                        }, 2000);


                                    }
                                }
                                }else {
                                     handleError(response.errorBody());
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonElement> call, Throwable t) {

                        }
                    });

                }
            }
        });
    }


    private void loginServiceRequest() {
        fragmentLogintwoBinding.btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLoginValidate()) {

                    String username = fragmentLogintwoBinding.etUsername.getText().toString();

                    String password = fragmentLogintwoBinding.etLoginpassword.getText().toString();

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.email = username;
                    loginRequest.password = password;

                    progressDoalog = new ProgressDialog(parent);
                    progressDoalog.setMessage("Loading....");
                    progressDoalog.show();


                    Call<JsonElement> call = serviceRequestInterface.loginRequest(loginRequest);

                    call.enqueue(new Callback<JsonElement>() {
                        @Override
                        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                            progressDoalog.dismiss();

                            try {

                                if(response.isSuccessful()) {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());

                                    Log.d("resultJson", jsonObject.toString());

                                    if (!jsonObject.has("success")) {
                                        LoginVo loginVo = com.webappclouds.utilslib.Utils.getSpecificVo(jsonObject.toString(),
                                                LoginVo.class);

                                        if (loginVo.getStatus().equalsIgnoreCase("true")) {
                                            AppPref.getInstance().saveAuthToken(loginVo.getUser().getApiToken());
                                            Intent intent = new Intent(parent, HomeActivity.class);
                                            startActivity(intent);
                                        } else {
                                            Utils.setSnackBar(fragmentLogintwoBinding.btnSignin, loginVo.getMessage());
                                        }
                                    }
                                }else {

                                    Utils.setSnackBar(fragmentLogintwoBinding.btnSignin,getResources().getString(R.string._txt_invalid));


                                }


                            } catch (JSONException e) {
                                Utils.setSnackBar(fragmentLogintwoBinding.btnSignin,getResources().getString(R.string.txt_somethingwentwrong));
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(Call<JsonElement> call, Throwable t) {
                            progressDoalog.dismiss();

                            Log.d("Throwable", t.toString());
                            Utils.setSnackBar(fragmentLogintwoBinding.btnSignin, parent.getResources().getString(R.string.txt_somethingwentwrong));

                        }
                    });


                }


            }
        });
    }

    public void handleError(ResponseBody responseBody) {
        String message = null;
        if (responseBody != null) {
            try {
                ErrorResponse errorResponse = new Gson().fromJson(responseBody.charStream(), ErrorResponse.class);
                message = errorResponse.error;
            } catch (JsonSyntaxException e) {
            } catch (JsonIOException e) {
            }
        }

        message = TextUtils.isEmpty(message) ? getString(R.string.txt_somethingwentwrong) : message;
        Utils.setSnackBar(fragmentLogintwoBinding.btnRegister,message);
    }

    private void clearFields() {
        fragmentLogintwoBinding.etName.setText("");
        fragmentLogintwoBinding.etEmail.setText("");
        fragmentLogintwoBinding.etPassword.setText("");
        fragmentLogintwoBinding.etConfirmPassword.setText("");
        fragmentLogintwoBinding.etPhone.setText("");
        fragmentLogintwoBinding.etGender.setText("");

    }

    private void setGenderDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(parent);

        builderSingle.setTitle("Select Gender");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Male");
        arrayAdapter.add("Female");
        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String strName = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(parent);
                builderInner.setMessage(strName);
                builderInner.setTitle("Your Selected Item is");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        fragmentLogintwoBinding.etGender.setText(strName);
                    }
                });
                builderInner.show();
            }
        });
        builderSingle.show();

    }




}
