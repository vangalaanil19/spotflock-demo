package com.spotfolcks.demo;

import com.google.gson.JsonElement;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceRequestInterface {



    @POST("login")
    Call<JsonElement> loginRequest(@Body LoginRequest loginRequest);


    @Headers("X-Requested-With:XMLHttpRequest")
    @POST("register")
    Call<JsonElement> signUpRequest(@Body SignUpRequest signUpRequest);


    @GET("kstream")
    Call<JsonElement> feedRequest();


}
