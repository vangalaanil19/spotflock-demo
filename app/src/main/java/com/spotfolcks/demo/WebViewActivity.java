package com.spotfolcks.demo;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.spotfolcks.demo.databinding.ActivityWebviewBinding;

import java.util.Objects;

public class WebViewActivity extends AppCompatActivity {
    ActivityWebviewBinding activityWebviewBinding;

    String feedUrl = "";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityWebviewBinding = DataBindingUtil.setContentView(this,R.layout.activity_webview);

        feedUrl = Objects.requireNonNull(getIntent().getExtras()).getString("webviewUrl");
        activityWebviewBinding.wvFeed.setWebViewClient(new WebViewClient());
        activityWebviewBinding.wvFeed.getSettings().setJavaScriptEnabled(true);
        activityWebviewBinding.wvFeed.loadUrl(feedUrl);
        activityWebviewBinding.wvFeed.setHorizontalScrollBarEnabled(false);

        WebSettings settings = activityWebviewBinding.wvFeed.getSettings();
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setDomStorageEnabled(true);
        activityWebviewBinding.wvFeed.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        activityWebviewBinding.wvFeed.setScrollbarFadingEnabled(true);

    }
}
