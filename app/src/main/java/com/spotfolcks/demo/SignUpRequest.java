package com.spotfolcks.demo;

public class SignUpRequest {

   public String name;
   public String email;
   public String password;
   public String password_confirmation;
   public String mobile;
   public String gender;
}
