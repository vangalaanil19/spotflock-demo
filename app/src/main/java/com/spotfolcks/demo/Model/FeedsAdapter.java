package com.spotfolcks.demo.Model;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.spotfolcks.demo.R;
import com.spotfolcks.demo.WebViewActivity;
import com.spotfolcks.demo.databinding.ItemFeedsBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.MyViews> {

    private Context context;

    private ArrayList<Datum> datumArrayList = new ArrayList<>();

    public FeedsAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<Datum> datumList) {
        this.datumArrayList.clear();
        this.datumArrayList.addAll(datumList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MyViews onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemFeedsBinding itemFeedsBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_feeds, viewGroup, false);
        return new FeedsAdapter.MyViews(itemFeedsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViews myViews, final int i) {
        myViews.itemFeedsBinding.tvTitle.setText(datumArrayList.get(i).getTitle());
        myViews.itemFeedsBinding.tvDescription.setText(datumArrayList.get(i).getFullDescription());

        Picasso.get().load(datumArrayList.get(i).getDescriptionImageUrl()).placeholder(context.getResources().getDrawable(R.drawable.icon_app_image)).into(myViews.itemFeedsBinding.ivFeed);


        String likes = String.valueOf(datumArrayList.get(i).getLikes()).concat("likes");
        myViews.itemFeedsBinding.tvLikes.setText(likes);

        String comments = String.valueOf(datumArrayList.get(i).getComments()).concat("comments");
        myViews.itemFeedsBinding.tvComment.setText(comments);

        String shares = String.valueOf(datumArrayList.get(i).getShares()).concat("shares");
        myViews.itemFeedsBinding.tvComment.setText(shares);

        myViews.itemFeedsBinding.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("webviewUrl",datumArrayList.get(i).getArticleUrl());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class MyViews extends RecyclerView.ViewHolder {
        ItemFeedsBinding itemFeedsBinding;

        public MyViews(ItemFeedsBinding itemFeedsBinding) {
            super(itemFeedsBinding.getRoot());
            this.itemFeedsBinding = itemFeedsBinding;
        }
    }
}
