package com.spotfolcks.demo;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.spotfolcks.demo.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {


    ActivityLoginBinding activityLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this,R.layout.activity_login);

        navigateLoginFragment();
    }


    private void navigateLoginFragment(){
        Bundle bundle = new Bundle();
        Utils.navigateFragment(new LoginFragment(),LoginFragment.TAG,bundle,this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
