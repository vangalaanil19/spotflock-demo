package com.spotfolcks.demo;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.spotfolcks.demo.databinding.ActivitySplashBinding;

public class SplashActivity extends AppCompatActivity {


    private static final int SPLASH_TIME = 2000;

    ActivitySplashBinding activitySplashBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding = DataBindingUtil.setContentView(this,R.layout.activity_splash);


        /*
        * Navigation from Splash screen to Login Screen
        * */

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        },SPLASH_TIME);

    }
}
