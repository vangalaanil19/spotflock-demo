package com.spotfolcks.demo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Datum implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("rss_source_id")
    @Expose
    private Integer rssSourceId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title_image")
    @Expose
    private Object titleImage;
    @SerializedName("tag_line")
    @Expose
    private Object tagLine;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("full_description")
    @Expose
    private String fullDescription;
    @SerializedName("title_image_url")
    @Expose
    private String titleImageUrl;
    @SerializedName("description_image_url")
    @Expose
    private String descriptionImageUrl;
    @SerializedName("article_url")
    @Expose
    private String articleUrl;
    @SerializedName("author")
    @Expose
    private Object author;
    @SerializedName("article_type")
    @Expose
    private String articleType;
    @SerializedName("published_date")
    @Expose
    private String publishedDate;
    @SerializedName("is_sponsored")
    @Expose
    private Integer isSponsored;
    @SerializedName("is_premium")
    @Expose
    private Integer isPremium;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("filtertags")
    @Expose
    private Object filtertags;
    @SerializedName("likes")
    @Expose
    private Integer likes;
    @SerializedName("comments")
    @Expose
    private Integer comments;
    @SerializedName("shares")
    @Expose
    private Integer shares;
    @SerializedName("meta_kstream_id")
    @Expose
    private Integer metaKstreamId;
    @SerializedName("accepted")
    @Expose
    private Integer accepted;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRssSourceId() {
        return rssSourceId;
    }

    public void setRssSourceId(Integer rssSourceId) {
        this.rssSourceId = rssSourceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(Object titleImage) {
        this.titleImage = titleImage;
    }

    public Object getTagLine() {
        return tagLine;
    }

    public void setTagLine(Object tagLine) {
        this.tagLine = tagLine;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getTitleImageUrl() {
        return titleImageUrl;
    }

    public void setTitleImageUrl(String titleImageUrl) {
        this.titleImageUrl = titleImageUrl;
    }

    public String getDescriptionImageUrl() {
        return descriptionImageUrl;
    }

    public void setDescriptionImageUrl(String descriptionImageUrl) {
        this.descriptionImageUrl = descriptionImageUrl;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public Object getAuthor() {
        return author;
    }

    public void setAuthor(Object author) {
        this.author = author;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Integer getIsSponsored() {
        return isSponsored;
    }

    public void setIsSponsored(Integer isSponsored) {
        this.isSponsored = isSponsored;
    }

    public Integer getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(Integer isPremium) {
        this.isPremium = isPremium;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Object getFiltertags() {
        return filtertags;
    }

    public void setFiltertags(Object filtertags) {
        this.filtertags = filtertags;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public Integer getShares() {
        return shares;
    }

    public void setShares(Integer shares) {
        this.shares = shares;
    }

    public Integer getMetaKstreamId() {
        return metaKstreamId;
    }

    public void setMetaKstreamId(Integer metaKstreamId) {
        this.metaKstreamId = metaKstreamId;
    }

    public Integer getAccepted() {
        return accepted;
    }

    public void setAccepted(Integer accepted) {
        this.accepted = accepted;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("rssSourceId", rssSourceId).append("title", title).append("titleImage", titleImage).append("tagLine", tagLine).append("shortDescription", shortDescription).append("fullDescription", fullDescription).append("titleImageUrl", titleImageUrl).append("descriptionImageUrl", descriptionImageUrl).append("articleUrl", articleUrl).append("author", author).append("articleType", articleType).append("publishedDate", publishedDate).append("isSponsored", isSponsored).append("isPremium", isPremium).append("tags", tags).append("filtertags", filtertags).append("likes", likes).append("comments", comments).append("shares", shares).append("metaKstreamId", metaKstreamId).append("accepted", accepted).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }
}
