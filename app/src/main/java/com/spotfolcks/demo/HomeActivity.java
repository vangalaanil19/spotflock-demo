package com.spotfolcks.demo;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.JsonElement;
import com.spotfolcks.demo.Model.FeedsAdapter;
import com.spotfolcks.demo.Model.FeedsVo;
import com.spotfolcks.demo.databinding.ActivityHomeBinding;
import com.webappclouds.utilslib.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    ActivityHomeBinding activityHomeBinding;

    ServiceRequestInterface serviceRequestInterface;

    FeedsAdapter feedsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityHomeBinding = DataBindingUtil.setContentView(this,R.layout.activity_home);
        serviceRequestInterface = ServerApi.getRetrofitInstance().create(ServiceRequestInterface.class);


        doServiceCall();

        feedsAdapter= new FeedsAdapter(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        activityHomeBinding.rvFeeds.setLayoutManager(layoutManager);

        activityHomeBinding.rvFeeds.setAdapter(feedsAdapter);
    }


    private void doServiceCall(){


        Call<JsonElement> call = serviceRequestInterface.feedRequest();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                Log.d("resultJson", response.body().toString());

                if(response.isSuccessful()){

                    FeedsVo feedsVo = Utils.getSpecificVo(response.body().toString(),FeedsVo.class);

                    if(feedsVo.getSuccess()){
                        feedsAdapter.setData(feedsVo.getKstream().getData());
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });




    }
}
