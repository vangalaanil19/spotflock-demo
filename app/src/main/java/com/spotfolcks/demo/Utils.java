package com.spotfolcks.demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import java.util.regex.Pattern;

public class Utils {
    public static void navigateFragment(Fragment fragment, String tag, Bundle bundle, FragmentActivity fragmentActivity) {
        FragmentManager fragmentManager = fragmentActivity
                .getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        fragmentTransaction.replace(R.id.main_frame, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    public static boolean haveInternet(Context context) {
        NetworkInfo info = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            return false;
        }
        if (info.isRoaming()) {
            // here is the roaming option you can change it if you want to
            // disable internet while roaming, just return false
            return true;
        }
        return true;
    }

    public static void savePreferences(Context mContext, String key,
                                       String value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                mContext.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void saveBooleanPrefernece(Context mContext, String key,
                                             boolean value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                mContext.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static String loadPreferences(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                mContext.getPackageName(), Context.MODE_PRIVATE);
        String val = "";
        try {
            val = sharedPreferences.getString(key, "");
        } catch (Exception e) {
            val = "";
        }
        return val;
    }

    public static boolean loadbooleanPreferneces(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                mContext.getPackageName(), Context.MODE_PRIVATE);
        boolean val = false;
        try {
            val = sharedPreferences.getBoolean(key, false);
        } catch (Exception e) {
            val = false;
        }
        return val;
    }

    static String pattern = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$";

    public static boolean emailValidate(String email) {
        if (email.length() == 0)
            return false;
        return Pattern.matches(pattern, email);
    }

    public static void setSnackBar(View view, String message) {

        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

}
