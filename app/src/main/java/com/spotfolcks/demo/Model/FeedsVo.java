package com.spotfolcks.demo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.spotfolcks.demo.Model.Kstream;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class FeedsVo implements Serializable {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("kstream")
    @Expose
    private Kstream kstream;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Kstream getKstream() {
        return kstream;
    }

    public void setKstream(Kstream kstream) {
        this.kstream = kstream;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("success", success).append("kstream", kstream).toString();
    }
}
